﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SparkTree.Web.Utils.Models.Abstract;

namespace SparkTree.Web.Utils.Models
{
    public class StatusModel : IStatusModel
    {
        public LibConstants.StatusModelType Status { get; set; }
        public List<String> ErrorList { get; set; }
        public String Title { get; set; }

        public StatusModel()
        {
            Status = LibConstants.StatusModelType.None;
            ErrorList = new List<string>();
        }

        public String GetErrorString()
        {
            String errString = String.Empty;
            ErrorList.ForEach(x => errString += x);
            return errString;
        }
    }
}
