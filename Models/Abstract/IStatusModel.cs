﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SparkTree.Web.Utils.Models.Abstract
{
    public interface IStatusModel
    {
        LibConstants.StatusModelType Status { get; set; }
        List<String> ErrorList { get; set; }
        
        String GetErrorString();
    }
}
