﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SparkTree.Web.Utils
{
    public static class LibConstants
    {
        public static String SystemName = "SYSTEM";

        public enum Month
        {
            None = 0,
            January = 1,
            February = 2,
            March = 3,
            April = 4,
            May = 5,
            June = 6,
            July = 7,
            August = 8,
            September = 9,
            October = 10,
            November = 11,
            December = 12
        }
        [Flags]
        public enum StatusModelType
        {
            None = 0,
            NotFoundErr = 1,
            PermissionErr = 2,
            StorageErr = 4, 
            DatabaseErr = 8,
            EmailErr = 16,
            LDAPErr = 32,
            HtmlErr = 64,
            ParameterErr = 128,
            CatalogErr = 256
        }

    }
}
