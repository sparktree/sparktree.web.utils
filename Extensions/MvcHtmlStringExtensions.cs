﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace SparkTree.Web.Utils.Extensions
{
    public static class MvcHtmlStringExtensions
    {
        public static MvcHtmlString If(this MvcHtmlString value, bool check)
        {
            if (check)
            {
                return value;
            }

            return null;
        }

        public static MvcHtmlString Else(this MvcHtmlString value, MvcHtmlString alternate)
        {
            if (value == null)
            {
                return alternate;
            }

            return value;
        }
    }
}
