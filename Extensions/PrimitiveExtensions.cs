﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace SparkTree.Web.Utils.Extensions
{
    public static class PrimitiveExtensions
    {
        /// <summary>
        /// Returns true if this String is neither null or empty.
        /// </summary>
        public static bool HasValue(this string s)
        {
            return !string.IsNullOrEmpty(s);
        }

        /// <summary>
        /// Convenience wrapper for static string.IsNullOrEmpty
        /// </summary>
        public static bool IsNullOrEmpty(this string s)
        {
            return string.IsNullOrEmpty(s);
        }

        /// <summary>
        /// Truncate String to Length.
        /// </summary>
            public static string Truncate(this string value, int maxLength)
            {
                return value.Length <= maxLength ? value : value.Substring(0, maxLength)+"...";
            }



        /// <summary>
        /// Encodes the to64.
        /// </summary>
        /// <param name="toEncode">To encode.</param>
        /// <returns></returns>
         public static string EncodeTo64(this string toEncode)
        {
            byte[] toEncodeAsBytes
                  = System.Text.Encoding.UTF8.GetBytes(toEncode);
            string returnValue
                  = System.Convert.ToBase64String(toEncodeAsBytes);
            return returnValue;
        }
         /// <summary>
         /// Decodes the from64.
         /// </summary>
         /// <param name="encodedData">The encoded data.</param>
         /// <returns></returns>
        public static string DecodeFrom64(this string encodedData)
        {
            byte[] encodedDataAsBytes
                = System.Convert.FromBase64String(encodedData);
            string returnValue =
               System.Text.Encoding.UTF8.GetString(encodedDataAsBytes);
            return returnValue;
        }
        public static string EncodeToUrl64(this String toEncode)
        {
            return toEncode.EncodeTo64().Replace("=", String.Empty).Replace('+', '-').Replace('/', '_');
        }
        public static string DecodeFromUrl64(this String encodedData)
        {
            encodedData = encodedData.PadRight(encodedData.Length + (4 - encodedData.Length % 4) % 4, '=');
            encodedData = encodedData.Replace('-', '+').Replace('_', '/');
            return encodedData.DecodeFrom64();
        }

    }

    /// <summary>
    /// The dictionary extensions.
    /// </summary>
    public static class DictionaryExtensions
    {
        /// <summary>
        /// Adds the value from an XDocument with the specified element name if it's not empty.
        /// </summary>
        /// <param name="dictionary">
        /// The dictionary. 
        /// </param>
        /// <param name="document">
        /// The document. 
        /// </param>
        /// <param name="elementName">
        /// Name of the element. 
        /// </param>
        public static void AddDataIfNotEmpty(
            this Dictionary<string, string> dictionary, XDocument document, string elementName)
        {
            var element = document.Root.Element(elementName);
            if (element != null)
            {
                dictionary.AddItemIfNotEmpty(elementName, element.Value);
            }
        }

        /// <summary>
        /// Adds a key/value pair to the specified dictionary if the value is not null or empty.
        /// </summary>
        /// <param name="dictionary">
        /// The dictionary. 
        /// </param>
        /// <param name="key">
        /// The key. 
        /// </param>
        /// <param name="value">
        /// The value. 
        /// </param>
        public static void AddItemIfNotEmpty(this IDictionary<string, string> dictionary, string key, string value)
        {
            if (key == null)
            {
                throw new ArgumentNullException("key");
            }

            if (!string.IsNullOrEmpty(value))
            {
                dictionary[key] = value;
            }
        }
    }
}
