﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SparkTree.Web.Utils.Extensions
{
    public static class EnumExtensions
    {
        public static String ToSplitString(this Enum enumType)
        {
            return enumType.ToString().Replace("_", " ");
        }
        
    }
}
