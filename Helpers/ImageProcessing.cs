﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SparkTree.Web.Utils.Helpers
{
    //public class ImageProcessing
    //{

    //    private static void Thumbnail(Stream source, Stream destination, int maxWidth, int maxHeight)
    //    {
    //        int width = 0, height = 0;
    //        BitmapFrame frame = null;
    //        try
    //        {
    //            frame = BitmapDecoder.Create(source, BitmapCreateOptions.None, BitmapCacheOption.None).Frames[0];
    //            width = frame.PixelWidth;
    //            height = frame.PixelHeight;
    //        }
    //        catch
    //        {
    //            throw new MyException("The image file is not in any of the supported image formats.");
    //        }

    //        if (width > AbsoluteLargestUploadWidth || height > AbsoluteLargestUploadHeight)
    //            throw new MyException("This image is too large");

    //        try
    //        {
    //            int targetWidth, targetHeight;
    //            ResizeWithAspect(width, height, maxWidth, maxHeight, out targetWidth, out targetHeight);

    //            BitmapFrame targetFrame;
    //            if (frame.PixelWidth == targetWidth && frame.PixelHeight == targetHeight)
    //                targetFrame = frame;
    //            else
    //            {
    //                var group = new DrawingGroup();
    //                RenderOptions.SetBitmapScalingMode(group, BitmapScalingMode.HighQuality);
    //                group.Children.Add(new ImageDrawing(frame, new Rect(0, 0, targetWidth, targetHeight)));
    //                var targetVisual = new DrawingVisual();
    //                var targetContext = targetVisual.RenderOpen();
    //                targetContext.DrawDrawing(group);
    //                var target = new RenderTargetBitmap(targetWidth, targetHeight, 96, 96, PixelFormats.Default);
    //                targetContext.Close();
    //                target.Render(targetVisual);
    //                targetFrame = BitmapFrame.Create(target);
    //            }

    //            var enc = new JpegBitmapEncoder();
    //            enc.Frames.Add(targetFrame);
    //            enc.QualityLevel = 80;
    //            enc.Save(destination);
    //        }
    //        catch
    //        {
    //            throw new MyException("The image file appears to be corrupt.");
    //        }
    //    }

    //    /// <summary>Generic helper to compute width/height that fit into specified maxima while preserving aspect ratio.</summary>
    //    public static void ResizeWithAspect(int origWidth, int origHeight, int maxWidth, int maxHeight, out int sizedWidth, out int sizedHeight)
    //    {
    //        if (origWidth < maxWidth && origHeight < maxHeight)
    //        {
    //            sizedWidth = origWidth;
    //            sizedHeight = origHeight;
    //            return;
    //        }

    //        sizedWidth = maxWidth;
    //        sizedHeight = (int)((double)origHeight / origWidth * sizedWidth + 0.5);
    //        if (sizedHeight > maxHeight)
    //        {
    //            sizedHeight = maxHeight;
    //            sizedWidth = (int)((double)origWidth / origHeight * sizedHeight + 0.5);
    //        }
    //    }
    //}
}
