﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SparkTree.Web.Utils.Helpers
{
    public class JavaScriptException : Exception
    {
        public JavaScriptException(string message)
            : base(message)
        {
        }
    }

    public class PaymentException : Exception
    {
        public PaymentException(string message)
            : base(message)
        {
        }
    }
}
