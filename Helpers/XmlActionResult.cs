﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;

namespace SparkTree.Web.Utils.Helpers
{
    public class XmlActionResult: ActionResult
    {
        public XDocument Xml { get; private set; }
        public string ContentType { get; set; }
        public Encoding Encoding { get; set; }

        public XmlActionResult(XDocument xml)
        {
            this.Xml = xml;
            this.ContentType = "text/xml;charset=utf-8";
            this.Encoding = Encoding.UTF8;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            context.HttpContext.Response.ContentType = this.ContentType;
            context.HttpContext.Response.HeaderEncoding = this.Encoding;


            var settings = new XmlWriterSettings {Encoding = new UTF8Encoding(false),  Indent = true, IndentChars = "\t"};
            // The false means, do not emit the BOM.
            using (XmlWriter w = XmlWriter.Create(context.HttpContext.Response.OutputStream, settings))
            {
                Xml.WriteTo(w);
            }

        }
    }
}
