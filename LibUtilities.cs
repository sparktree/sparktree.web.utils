﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using System.Web.Hosting;
using System.Web.Security;

namespace SparkTree.Web.Utils
{
    public static class LibUtilities
    {
        public static void ClearAuthState()
        {
            //FormsAuthentication.SignOut();
            FormsAuthentication.SignOut();
            Roles.DeleteCookie();
            HttpContext.Current.Session.Clear();
        }
        #region File Path Utilities


        /// <summary>
        /// Returns the full system path to an item located in the App_Data folder.
        /// All file paths should be file paths, seperated by '\' characters.
        /// </summary>
        /// <param name="itemName">Name of the item.</param>
        /// <returns></returns>
        public static String AppDataFolderItemPath(String itemName)
        {
            return Path.Combine(HostingEnvironment.ApplicationPhysicalPath, @"App_Data\", itemName);
        }

        /// <summary>
        /// Generates a unique filename by prepending a GUID to the given filename.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        public static String GenerateUniqueFileName(String fileName)
        {
            return GenerateSimpleGuid() + fileName;
        }

        /// <summary>
        /// Sanitizes the name of the file.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public static string SanitizeFileName(string name)
        {
            string invalidChars = Regex.Escape(new string(ApplicationInvalidFileNameChars()));
            string invalidReStr = string.Format(@"[{0}]+", invalidChars);
            return Regex.Replace(name, invalidReStr, "_").TrimEnd('.');
        }
        private static char[] ApplicationInvalidFileNameChars()
        {
            return Path.GetInvalidFileNameChars().Concat(new[] {',', '&','>','<','?','#' }).ToArray();
        }

        #endregion


        #region Identitifer Utilities

        /// <summary>
        /// Generates a new Simple Guid without dashes.
        /// </summary>
        /// <returns></returns>
        public static String GenerateSimpleGuid()
        {
            return Guid.NewGuid().ToString("N");
        }





        #endregion

        #region Request Context Storage

        /// <summary>
        /// Get something from the per-request cache.
        /// 
        /// Returns null if not found.
        /// </summary>
        public static T GetFromContext<T>(string name) where T : class
        {
            return HttpContext.Current.Items[name] as T;
        }

        /// <summary>
        /// Place something in the per-request cache.
        /// 
        /// Once this HttpRequest is complete, it will be lost.
        /// 
        /// Reference types only.
        /// </summary>
        public static void SetInContext<T>(string name, T value) where T : class
        {
            HttpContext.Current.Items[name] = value;
        }

        #endregion

    }
}
